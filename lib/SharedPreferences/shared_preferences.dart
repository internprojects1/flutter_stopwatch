import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Storage {
  final String timeList = "timeList";
  final String favouriteList = "favouriteList";

  Future<bool> setTimeList(List<String> value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList(timeList, value);
  }

  Future<List<String>> getTimeList() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> list = prefs.getStringList(timeList);
    // if (list != null && list is! List<String>) {
    //   list = list.cast<String>().toList();
    // }
    return list;
  }

  Future<bool> setFavouriteList(List<String> value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList(favouriteList, value);
  }

  Future<List<String>> getFavouriteList() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> list = prefs.getStringList(favouriteList);
    // if (list != null && list is! List<String>) {
    //   list = list.cast<String>().toList();
    // }
    return list;
  }
}
