import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:stock/Globals/global.dart' as global;

class Favourites extends StatefulWidget {
  @override
  _FavouritesState createState() => _FavouritesState();
}

class _FavouritesState extends State<Favourites> {
  //var timeList = global.timeList;
  var favouriteList = global.favouriteList;
  String deletedTime, fav_del_time, removedFavouriteTime;

  void removeFavourite(index) {
    setState(() {
      removedFavouriteTime = favouriteList.removeAt(index);
    });
    Scaffold.of(context).showSnackBar(
      new SnackBar(
        content: new Text("$removedFavouriteTime removed from favourites"),
        action: SnackBarAction(
          label: "UNDO",
          onPressed: () {
            setState(() {
              favouriteList.insert(index, removedFavouriteTime);
              print(favouriteList);
            });
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        child: ListView.builder(
          itemCount: favouriteList.length,
          itemBuilder: (context, index) {
            return Slidable(
              actionPane: SlidableDrawerActionPane(),
              actionExtentRatio: 0.25,
              child: Container(
                color: Colors.white,
                child: ListTile(
                  leading: Icon(Icons.access_time),
                  title: Text(favouriteList[index]),
                ),
              ),
              secondaryActions: <Widget>[
                IconSlideAction(
                  caption: 'Remove',
                  color: Colors.red,
                  icon: Icons.delete_outline,
                  onTap: () => removeFavourite(index),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
