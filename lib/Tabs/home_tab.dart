import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:stock/Globals/global.dart' as global;
import 'package:stock/SharedPreferences/shared_preferences.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  bool isStart = true, isStarted = false;
  String bt = "START", displayTime = "00:00:00";
  Storage storage = Storage();

  var time = Stopwatch();
  var duration = const Duration(seconds: 1);

  void startTimer() {
    Timer(duration, running);
  }

  void running() {
    if (time.isRunning) {
      startTimer();
    }
    setState(() {
      displayTime = time.elapsed.inHours.toString().padLeft(2, "0") +
          ":" +
          (time.elapsed.inMinutes % 60).toString().padLeft(2, "0") +
          ":" +
          (time.elapsed.inSeconds % 60).toString().padLeft(2, "0");
    });
  }

  void startCount() {
    setState(() {
      isStart = false;
      isStarted = true;
      bt = "STOP";
    });
    time.start();
    startTimer();
  }

  void stopCount() {
    setState(() {
      isStart = true;
      bt = "START";
    });
    time.stop();
  }

  void resetCount() {
    setState(() {
      isStarted = false;
    });
    time.reset();
    displayTime = "00:00:00";
  }

  void recordTime() {
    global.timeList.add(displayTime);
    //storage.setTimeList(global.timeList);
    print(global.timeList);
    Fluttertoast.showToast(
        msg: "Time recorded",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.grey,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(height: 20),
          Container(
            alignment: Alignment.topCenter,
            child: Text(
              displayTime,
              style: TextStyle(fontSize: 50.0, fontWeight: FontWeight.w900),
            ),
          ),
          SizedBox(height: 20),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new RaisedButton(
                  padding: const EdgeInsets.all(8.0),
                  textColor: Colors.white,
                  color: isStart ? Colors.green : Colors.red,
                  onPressed: isStart ? startCount : stopCount,
                  child: new Text(bt),
                ),
                new RaisedButton(
                  padding: const EdgeInsets.all(8.0),
                  textColor: Colors.white,
                  color: Colors.orange,
                  onPressed: isStarted ? resetCount : null,
                  child: new Text("RESET"),
                ),
                new RaisedButton(
                  padding: const EdgeInsets.all(8.0),
                  textColor: Colors.white,
                  color: Colors.indigoAccent,
                  onPressed: isStarted ? recordTime : null,
                  child: new Text("TAG"),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
