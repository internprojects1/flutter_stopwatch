import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:stock/Globals/global.dart' as global;

class OperationsTab extends StatefulWidget {
  @override
  _OperationsTabState createState() => _OperationsTabState();
}

class _OperationsTabState extends State<OperationsTab> {
  var timeList = global.timeList;
  var favouriteList = global.favouriteList;
  String deletedTime, fav_del_time;

  void addToFavourite(index) {
    setState(() {
      fav_del_time = timeList[index];
      favouriteList.add(fav_del_time);
      print(favouriteList);
    });
    Scaffold.of(context).showSnackBar(
      new SnackBar(
        content: new Text("$fav_del_time added to favourites"),
        action: SnackBarAction(
          label: "UNDO",
          onPressed: () {
            setState(() {
              favouriteList.remove(fav_del_time);
              print(favouriteList);
            });
          },
        ),
      ),
    );
  }

  void removeTime(index) {
    setState(() {
      deletedTime = timeList.removeAt(index);
    });
    Scaffold.of(context).showSnackBar(
      new SnackBar(
        content: new Text("$deletedTime Record removed"),
        action: SnackBarAction(
          label: "UNDO",
          onPressed: () {
            setState(() {
              timeList.insert(index, deletedTime);
              print(timeList);
            });
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        child: ListView.builder(
          itemCount: timeList.length,
          itemBuilder: (context, index) {
            return Slidable(
              actionPane: SlidableDrawerActionPane(),
              actionExtentRatio: 0.25,
              child: Container(
                color: Colors.white,
                child: ListTile(
                  leading: Icon(Icons.access_time),
                  title: Text(timeList[index]),
                ),
              ),
              actions: <Widget>[
                IconSlideAction(
                  foregroundColor: Colors.white,
                  caption: 'Add to favourites',
                  color: Colors.yellow[900],
                  icon: Icons.star_border,
                  onTap: () => addToFavourite(index),
                ),
              ],
              secondaryActions: <Widget>[
                IconSlideAction(
                  caption: 'Delete',
                  color: Colors.red,
                  icon: Icons.delete_outline,
                  onTap: () => removeTime(index),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
