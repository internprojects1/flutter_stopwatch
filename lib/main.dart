import 'dart:async';

import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:stock/tabs/favourites_tab.dart';
import './tabs/home_tab.dart';
import './tabs/operations_tab.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.purple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(
        seconds: 3,
        backgroundColor: Colors.purple,
        navigateAfterSeconds: HomeBar(),
      ),
    );
  }
}

class HomeBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text("TimeTask"),
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.home), text: "Home"),
              Tab(icon: Icon(Icons.access_time), text: "Records"),
              Tab(icon: Icon(Icons.star_border), text: "Favourites")
            ],
          ),
        ),
        body: TabBarView(
          children: [HomeTab(), OperationsTab(), Favourites()],
        ),
      ),
    );
  }
}
